package discovery

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"time"

)

// Registry defines a service registery.
type Registry interface {
	// Register creates a service instance record in the registery.
	Register(ctx context.Context, instanceID, serviceName, hostPort string) error
	// Deregister removes a service instance record from the registery.
	Deregister(ctx context.Context, instanceID, serviceName string) error
	// ServiceAddresses returns the list of addresses of active instances of the given service.
	ServiceAddresses(ctx context.Context, serviceName string) ([]string, error)
	// ReportHealthyState is a push mechanism for reporting healthy state to the registry.
	ReportHealthyState(ctx context.Context, instanceID, serviceName string) error
}

// ErrNotFound is returned when no service addresses are found.
var ErrNotFound = errors.New("no service addresses found")

// GenerateInstanceID generates a random service instance id, using a service name
// suffixed by dash and a random number.
func GenerateInstanceID(serviceName string) string {
	return fmt.Sprintf("%s-%d", serviceName, rand.New(rand.NewSource(time.Now().UnixNano())).Int())
}

