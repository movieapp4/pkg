package memory

import (
	"context"
	"errors"
	"sync"
	"time"

	"gitlab.com/movieapp4/pkg/discovery"
)

type serviceName string
type instanceID string

type serviceInstance struct {
	hostPort   string
	lastActive time.Time
}

// Registry defines an in-memory service registry.
type Registry struct {
	sync.RWMutex
	serviceAddrs map[serviceName]map[instanceID]*serviceInstance
}

// NewRegistry creates a new in-memory service registry instance.
func NewRegistry() *Registry {
	return &Registry{serviceAddrs: map[serviceName]map[instanceID]*serviceInstance{}}
}

// Register creates a service record in the registry.
func (r *Registry) Register(ctx context.Context, instID string, servName string, hostPort string) error {

	r.Lock()
	defer r.Unlock()

	if _, ok := r.serviceAddrs[serviceName(servName)]; !ok {
		r.serviceAddrs[serviceName(servName)] = map[instanceID]*serviceInstance{}
	}

	r.serviceAddrs[serviceName(servName)][instanceID(instID)] = &serviceInstance{
		hostPort:   hostPort,
		lastActive: time.Now(),
	}

	return nil
}

// Deregister removes a service record from the registry.
func (r *Registry) Deregister(ctx context.Context, instID, serviserviceName string) error {

	r.Lock()
	defer r.Unlock()

	if _, ok := r.serviceAddrs[serviceName(serviserviceName)]; ok {
		delete(r.serviceAddrs[serviceName(serviserviceName)], instanceID(instID))
		return nil
	}

	return nil

}

// ReportHealthyState is a push mechanism for reporting healthy state to the registry.
func (r *Registry) ReportHealthyState(ctx context.Context, instID, servName string) error {

	r.Lock()
	defer r.Unlock()

	if _, ok := r.serviceAddrs[serviceName(servName)]; !ok {
		return errors.New("service is not registred")
	}

	if _, ok := r.serviceAddrs[serviceName(servName)][instanceID(instID)]; !ok {
		return errors.New("service instance is not registed yet")
	}

	r.serviceAddrs[serviceName(servName)][instanceID(instID)].lastActive = time.Now()

	return nil
}

// ServiceAddresses returns the list of addresses of active instances of the given service.
func (r *Registry) ServiceAddresses(ctx context.Context, servName string) ([]string, error) {
	r.Lock()
	defer r.Unlock()

	if len(r.serviceAddrs[serviceName(servName)]) == 0 {
		return nil, discovery.ErrNotFound
	}

	var resp []string

	for _, v := range r.serviceAddrs[serviceName(servName)] {

		if v.lastActive.Before(time.Now().Add(-5 * time.Second)) {
			continue
		}

		resp = append(resp, v.hostPort)
	}

	return resp, nil
}
